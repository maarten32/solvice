import unittest
import endpoints
import time


class TestRemote(unittest.TestCase):
    def test_endpoint(self):
        endpoints.set_endpoint_mode_remote()
        demo_body = endpoints.get_demo("shift").json()
        job_id = endpoints.post_job_get_id(demo_body, 1)
        time.sleep(3)
        json_result = endpoints.get_job(job_id).json()
        assert json_result is not None
        assert json_result['score']['hardScore'] != 0


class TestLocal(unittest.TestCase):
    def test_endpoint(self):
        endpoints.set_endpoint_mode_local()
        demo_body = endpoints.get_demo("shift").json()
        job_id = endpoints.post_job_get_id(demo_body, 1)
        time.sleep(2)
        json_result = endpoints.get_job(job_id).json()
        assert json_result is not None
        assert json_result['score']['hardScore'] != 0


if __name__ == '__main__':
    unittest.main()
