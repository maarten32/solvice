import requests
import config

ENDPOINT_MODE_REMOTE = "REMOTE"
ENDPOINT_MODE_LOCAL = "LOCAL"
ENDPOINT_MODE = ENDPOINT_MODE_REMOTE

header_keys = {"Authorization": config.API_KEY, "Content-Type": "application/json", "Accept": "application/json"}


def set_endpoint_mode_remote():
    global ENDPOINT_MODE
    ENDPOINT_MODE = ENDPOINT_MODE_REMOTE


def set_endpoint_mode_local():
    global ENDPOINT_MODE
    ENDPOINT_MODE = ENDPOINT_MODE_LOCAL


def resolve_uri_base():
    return config.get_local_uri_base() if ENDPOINT_MODE == ENDPOINT_MODE_LOCAL else config.get_remote_uri_base()


def get_demo_id(solver_name, uri_base=None):
    if uri_base is None: uri_base = resolve_uri_base()
    return get_demo(solver_name, uri_base).json()['id']


def get_demo(solver_name, uri_base=None):
    if uri_base is None: uri_base = resolve_uri_base()
    url = uri_base + "/demo/" + solver_name
    response = requests.get(url, headers=header_keys)
    return response


def post_job_get_id(json_request, solver_running_time, uri_base=None):
    if uri_base is None: uri_base = resolve_uri_base()
    return post_job(json_request, solver_running_time, uri_base).json()['id']


def post_job(json_request, solver_running_time, uri_base=None):
    if uri_base is None: uri_base = resolve_uri_base()
    url = uri_base + "/solve?seconds={}".format(solver_running_time)
    response = requests.post(url, headers=header_keys, json=json_request)
    return response


def evaluate_get_id(json_request, uri_base=None):
    if uri_base is None: uri_base = resolve_uri_base()
    return evaluate(json_request, uri_base).json()['id']


def evaluate(json_request, uri_base=None):
    if uri_base is None: uri_base = resolve_uri_base()
    url = uri_base + "/evaluate"
    response = requests.post(url, headers=header_keys, json=json_request)
    return response


def get_job(job_id, uri_base=None):
    if uri_base is None: uri_base = resolve_uri_base()
    url = uri_base + "/jobs/{}".format(job_id)
    response = requests.get(url, headers=header_keys)
    return response
